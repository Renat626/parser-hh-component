<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
    include_once __DIR__ . '/phpQuery-onefile.php';

    class vacancyComponent extends CBitrixComponent {
        public function pageResult() {
            $application = \Bitrix\Main\Application::getInstance();
            $context = $application->getContext();
            $request = $context->getRequest();

            $this->arResult["DATA"] = array(
                "NAME" => "",
                "EXPERIENCE" => array(),
                "PRICE" => "",
            );
            
            try {
                $doc = phpQuery::newDocument(file_get_contents($request["urlVacancy"]));
                $nameVacancy = $doc->find('h1');
                $namePrice = $doc->find('.vacancy-title div .bloko-header-section-2');
                $nameExprerience = $doc->find('.vacancy-description-list-item');
        
                $this->arResult["DATA"]["NAME"] = pq($nameVacancy)->text();
               
        
                foreach ($nameExprerience as $value) {
                    $ent = pq($value);
                    $name = $ent->text();
                    array_push($this->arResult["DATA"]["EXPERIENCE"], $name);
                }
        
                $this->arResult["DATA"]["PRICE"] = pq($namePrice)->text();

                $this->pageInsert();
            } catch (\Throwable $th) {
                $this->arResult["ERROR"] = "Введите ссылку на вакансию";
                // throw $th;
            }
        }

        public function pageSelect() {
            if (CModule::IncludeModule("iblock")) {
                $arrProperties = CIBlockProperty::GetList(
                    ["SORT" => "ASC"],
                    ["IBLOCK_ID" => $this->arParams["IBLOCK_ID"]]
                );

                $this->arResult["PROPERTIES_FOR_INSERT"] = [];

                while ($obj = $arrProperties->GetNext()) {
                    array_push($this->arResult["PROPERTIES_FOR_INSERT"], $obj);
                }

                $fields = ["NAME"];

                foreach ($this->arParams["PROPERTY_CODE"] as $value) {
                    if ($value != "") {
                        // array_push($fields, "PROPERTY_".$value."_ID");
                        // array_push($fields, "PROPERTY_".$value."_VALUE");
                        array_push($fields, "PROPERTY_".$value);
                    }
                }

                $this->arResult["TEST"] = $fields;

                $arr = CIBlockElement::GetList(
                    ["SORT" => "ASC"],
                    ["IBLOCK_ID" => $this->arParams["IBLOCK_ID"]],
                    false,
                    ["nTopCount" => $this->arParams["NEWS_COUNT"]],
                    $fields
                );

                $this->arResult["ITEMS"] = [];

                while ($obj = $arr->GetNext()) {
                    array_push($this->arResult["ITEMS"], $obj);
                }
            }
        }

        public function pageInsert() {
            try {
                $this->pageSelect();

                $el = new CIBlockElement;
                $PROP = [];

                foreach ($this->arResult["PROPERTIES_FOR_INSERT"] as $key => $value) {
                    if ($value["NAME"] == "Требуемый опыт работы") {
                        $PROP[$value["ID"]] = $this->arResult["DATA"]["EXPERIENCE"][0];
                    }
                    
                    if ($value["NAME"] == "Тип занятости") {
                        $PROP[$value["ID"]] = $this->arResult["DATA"]["EXPERIENCE"][1];
                    }

                    if ($value["NAME"] == "Зарплата") {
                        $PROP[$value["ID"]] = $this->arResult["DATA"]["PRICE"];
                    }
                }

                $arLoadProductArray = Array( // элемент изменен текущим пользователем
                "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
                "IBLOCK_ID"      => $this->arParams["IBLOCK_ID"],
                "PROPERTY_VALUES"=> $PROP,
                "NAME"           => $this->arResult["DATA"]["NAME"],
                "ACTIVE"         => "Y",            // активен
                );
                
                $PRODUCT_ID = $el->Add($arLoadProductArray);
            } catch (\Throwable $th) {
                $this->arResult["ERROR2"] = $th;
                // throw $th;
            }
        }

        public function executeComponent() {
            $this->pageResult();
            $this->pageSelect();

            $this->includeComponentTemplate();
        }
    }

    
    
    // $this->includeComponentTemplate();
?>